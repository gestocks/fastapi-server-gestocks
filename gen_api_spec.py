#!./env/bin/python

from fastapi.openapi.utils import get_openapi
from gestocks_server.main import app
import json


with open('openapi.json', 'w') as f:
    json.dump(get_openapi(
        title=app.title,
        version=app.version,
        openapi_version="3.0.3",
        description=app.description,
        routes=app.routes,
    ), f)