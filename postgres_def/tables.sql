BEGIN;
DROP TABLE IF EXISTS commentaire_instance;
DROP TABLE IF EXISTS commentaire_definition;
DROP TABLE IF EXISTS commentaire;
DROP TABLE IF EXISTS instance_composant;
DROP TABLE IF EXISTS definition_composant;
DROP TABLE IF EXISTS emplacement;
DROP TABLE IF EXISTS images;
DROP TABLE IF EXISTS session_tokken;
DROP TABLE IF EXISTS users;
DROP TYPE IF EXISTS der_rank;

CREATE TYPE der_rank AS ENUM(
    'SITE_ADMIN',
    'GESTION_MAGASIN',
    'GESTION_USERS',
    'GESTION_STOCKS',
    'USER'
    );

CREATE TABLE users(
    id SERIAL PRIMARY KEY,
    nom TEXT NOT NULL UNIQUE,
    psw_hash BYTEA NOT NULL,
    salt BYTEA NOT NULL,
    blocked BOOLEAN NOT NULL DEFAULT false,
    ranks der_rank[] NOT NULL DEFAULT '{"USER"}'
);

CREATE TABLE session_tokken(
    tokken TEXT PRIMARY KEY,
    users INTEGER NOT NULL REFERENCES users(id),
    perish_date TIMESTAMP NOT NULL DEFAULT NOW() + '1 hour',
    revoked BOOLEAN NOT NULL DEFAULT false
);

CREATE TABLE images(
    id SERIAL PRIMARY KEY,
    large BYTEA NOT NULL,
    small BYTEA NOT NULL
);

CREATE TABLE definition_composant (
    id SERIAL PRIMARY KEY,
    nom TEXT,
    id_image INTEGER REFERENCES images(id)
);

CREATE TABLE emplacement (
    id SERIAL PRIMARY KEY,
    nom TEXT
);

CREATE TABLE instance_composant(
    id SERIAL PRIMARY KEY,
    id_emplacement INTEGER NOT NULL REFERENCES emplacement(id),
    id_definition INTEGER NOT NULL REFERENCES definition_composant(id),
    id_image INTEGER REFERENCES images(id)
);

CREATE TABLE commentaire(
    id SERIAL PRIMARY KEY,
    contenu TEXT NOT NULL,
    auteur INTEGER REFERENCES users(id),
    timestamp TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE commentaire_definition(
    def INTEGER REFERENCES definition_composant(id)
) INHERITS (commentaire);

CREATE TABLE commentaire_instance(
    def INTEGER REFERENCES instance_composant(id)
) INHERITS (commentaire);

COMMIT;