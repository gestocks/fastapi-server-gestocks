# coding: utf-8

from fastapi.testclient import TestClient


from gestocks_server.models.contenu_emplacement import ContenuEmplacement  # noqa: F401
from gestocks_server.models.repartition_instance import RepartitionInstance  # noqa: F401


def test_composant_repartition_id_definition_get(client: TestClient):
    """Test case for composant_repartition_id_definition_get

    Recherche de la position d'un composant.
    """

    headers = {
        "page_number": 0,
        "page_size": 25,
    }
    response = client.request(
        "GET",
        "/Composant/repartition/{idDefinition}".format(idDefinition=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_emplacement_contenu_id_emplacement_get(client: TestClient):
    """Test case for emplacement_contenu_id_emplacement_get

    Getteur du contenu d'un emplacement.
    """

    headers = {
        "page_number": 0,
        "page_size": 25,
    }
    response = client.request(
        "GET",
        "/Emplacement/contenu/{idEmplacement}".format(idEmplacement=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200

