# coding: utf-8

from fastapi.testclient import TestClient


from gestocks_server.models.login_post200_response import LoginPost200Response  # noqa: F401


def test_login_post(client: TestClient):
    """Test case for login_post

    
    """

    headers = {
    }
    data = {
        "username": 'username_example',
        "password": 'password_example',
        "grant_type": 'grant_type_example',
        "scope": 'scope_example'
    }
    response = client.request(
        "POST",
        "/login",
        headers=headers,
        data=data,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_logout_get(client: TestClient):
    """Test case for logout_get

    
    """

    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(
        "GET",
        "/logout",
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200

