# coding: utf-8

from fastapi.testclient import TestClient


from gestocks_server.models.contenu_emplacement import ContenuEmplacement  # noqa: F401
from gestocks_server.models.emplacement import Emplacement  # noqa: F401


def test_emplacement_by_id_id_get(client: TestClient):
    """Test case for emplacement_by_id_id_get

    Accesseur d'un emplacement par son Id
    """

    headers = {
    }
    response = client.request(
        "GET",
        "/Emplacement/byId/{id}".format(id=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_emplacement_by_id_id_patch(client: TestClient):
    """Test case for emplacement_by_id_id_patch

    Update des emplacements
    """
    emplacement = {"id":101,"nom":"Placard de Pascal"}

    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(
        "PATCH",
        "/Emplacement/byId/{id}".format(id=56),
        headers=headers,
        json=emplacement,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_emplacement_contenu_id_emplacement_get(client: TestClient):
    """Test case for emplacement_contenu_id_emplacement_get

    Getteur du contenu d'un emplacement.
    """

    headers = {
        "page_number": 0,
        "page_size": 25,
    }
    response = client.request(
        "GET",
        "/Emplacement/contenu/{idEmplacement}".format(idEmplacement=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_emplacement_post(client: TestClient):
    """Test case for emplacement_post

    Création d'un nouvel emplacement.
    """
    emplacement = {"id":101,"nom":"Placard de Pascal"}

    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(
        "POST",
        "/Emplacement",
        headers=headers,
        json=emplacement,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_emplacements_get(client: TestClient):
    """Test case for emplacements_get

    Accesseur pour la liste des composants.
    """

    headers = {
        "page_number": 0,
        "page_size": 25,
    }
    response = client.request(
        "GET",
        "/Emplacements",
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200

