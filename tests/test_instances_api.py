# coding: utf-8

from fastapi.testclient import TestClient


from gestocks_server.models.commentaire import Commentaire  # noqa: F401
from gestocks_server.models.instance_composant import InstanceComposant  # noqa: F401


def test_instance_by_id_id_comments_get(client: TestClient):
    """Test case for instance_by_id_id_comments_get

    Accesseur à la liste des commentaires
    """

    headers = {
    }
    response = client.request(
        "GET",
        "/Instance/byId/{id}/comments".format(id=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_instance_by_id_id_comments_post(client: TestClient):
    """Test case for instance_by_id_id_comments_post

    Création d'un commentaire de instance
    """
    body = 'body_example'

    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(
        "POST",
        "/Instance/byId/{id}/comments".format(id=56),
        headers=headers,
        json=body,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_instance_by_id_id_delete(client: TestClient):
    """Test case for instance_by_id_id_delete

    Destructeur d'une instance par son Id.
    """

    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(
        "DELETE",
        "/Instance/byId/{id}".format(id=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_instance_by_id_id_get(client: TestClient):
    """Test case for instance_by_id_id_get

    Accesseur d'une instance par son Id
    """

    headers = {
    }
    response = client.request(
        "GET",
        "/Instance/byId/{id}".format(id=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_instance_by_id_id_image_get(client: TestClient):
    """Test case for instance_by_id_id_image_get

    
    """

    headers = {
    }
    response = client.request(
        "GET",
        "/Instance/byId/{id}/image".format(id=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_instance_by_id_id_image_post(client: TestClient):
    """Test case for instance_by_id_id_image_post

    
    """
    body = '/path/to/file'

    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(
        "POST",
        "/Instance/byId/{id}/image".format(id=56),
        headers=headers,
        json=body,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_instance_by_id_id_low_image_get(client: TestClient):
    """Test case for instance_by_id_id_low_image_get

    
    """

    headers = {
    }
    response = client.request(
        "GET",
        "/Instance/byId/{id}/low_image".format(id=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_instance_deplacement_id_instance_vers_id_emplacement_patch(client: TestClient):
    """Test case for instance_deplacement_id_instance_vers_id_emplacement_patch

    Mutateur de l'emplacement d'une instance.
    """

    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(
        "PATCH",
        "/Instance/deplacement/{id_instance}/vers/{id_emplacement}".format(id_instance=56, id_emplacement=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_instance_post(client: TestClient):
    """Test case for instance_post

    Création d'une nouvelle instance de composant.
    """
    instance_composant = {"id":420,"id_definition":42,"id_emplacement":101}

    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(
        "POST",
        "/Instance",
        headers=headers,
        json=instance_composant,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_instances_get(client: TestClient):
    """Test case for instances_get

    Accesseur à la liste des instances.
    """

    headers = {
        "page_number": 0,
        "page_size": 25,
    }
    response = client.request(
        "GET",
        "/Instances",
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200

