# coding: utf-8

from fastapi.testclient import TestClient




def test_composant_by_id_id_image_get(client: TestClient):
    """Test case for composant_by_id_id_image_get

    
    """

    headers = {
    }
    response = client.request(
        "GET",
        "/Composant/byId/{id}/image".format(id=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_composant_by_id_id_image_post(client: TestClient):
    """Test case for composant_by_id_id_image_post

    
    """
    body = '/path/to/file'

    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(
        "POST",
        "/Composant/byId/{id}/image".format(id=56),
        headers=headers,
        json=body,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_composant_by_id_id_low_image_get(client: TestClient):
    """Test case for composant_by_id_id_low_image_get

    
    """

    headers = {
    }
    response = client.request(
        "GET",
        "/Composant/byId/{id}/low_image".format(id=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_instance_by_id_id_image_get(client: TestClient):
    """Test case for instance_by_id_id_image_get

    
    """

    headers = {
    }
    response = client.request(
        "GET",
        "/Instance/byId/{id}/image".format(id=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_instance_by_id_id_image_post(client: TestClient):
    """Test case for instance_by_id_id_image_post

    
    """
    body = '/path/to/file'

    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(
        "POST",
        "/Instance/byId/{id}/image".format(id=56),
        headers=headers,
        json=body,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_instance_by_id_id_low_image_get(client: TestClient):
    """Test case for instance_by_id_id_low_image_get

    
    """

    headers = {
    }
    response = client.request(
        "GET",
        "/Instance/byId/{id}/low_image".format(id=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200

