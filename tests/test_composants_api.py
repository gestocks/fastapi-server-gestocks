# coding: utf-8

from fastapi.testclient import TestClient


from gestocks_server.models.commentaire import Commentaire  # noqa: F401
from gestocks_server.models.declaration_composant import DeclarationComposant  # noqa: F401
from gestocks_server.models.repartition_instance import RepartitionInstance  # noqa: F401


def test_composant_by_id_id_comments_get(client: TestClient):
    """Test case for composant_by_id_id_comments_get

    Accesseur à la liste des commentaires
    """

    headers = {
    }
    response = client.request(
        "GET",
        "/Composant/byId/{id}/comments".format(id=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_composant_by_id_id_comments_post(client: TestClient):
    """Test case for composant_by_id_id_comments_post

    Création d'un commentaire de composant
    """
    body = 'body_example'

    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(
        "POST",
        "/Composant/byId/{id}/comments".format(id=56),
        headers=headers,
        json=body,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_composant_by_id_id_get(client: TestClient):
    """Test case for composant_by_id_id_get

    Accesseur d'une définition de composant par son Id
    """

    headers = {
    }
    response = client.request(
        "GET",
        "/Composant/byId/{id}".format(id=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_composant_by_id_id_image_get(client: TestClient):
    """Test case for composant_by_id_id_image_get

    
    """

    headers = {
    }
    response = client.request(
        "GET",
        "/Composant/byId/{id}/image".format(id=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_composant_by_id_id_image_post(client: TestClient):
    """Test case for composant_by_id_id_image_post

    
    """
    body = '/path/to/file'

    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(
        "POST",
        "/Composant/byId/{id}/image".format(id=56),
        headers=headers,
        json=body,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_composant_by_id_id_low_image_get(client: TestClient):
    """Test case for composant_by_id_id_low_image_get

    
    """

    headers = {
    }
    response = client.request(
        "GET",
        "/Composant/byId/{id}/low_image".format(id=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_composant_post(client: TestClient):
    """Test case for composant_post

    Création d'un nouveaux type de composants.
    """
    declaration_composant = {"id":42,"nom":"RaspberryPI4"}

    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(
        "POST",
        "/Composant",
        headers=headers,
        json=declaration_composant,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_composant_repartition_id_definition_get(client: TestClient):
    """Test case for composant_repartition_id_definition_get

    Recherche de la position d'un composant.
    """

    headers = {
        "page_number": 0,
        "page_size": 25,
    }
    response = client.request(
        "GET",
        "/Composant/repartition/{idDefinition}".format(idDefinition=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200


def test_composants_get(client: TestClient):
    """Test case for composants_get

    Accesseur pour la liste des definitions.
    """

    headers = {
        "page_number": 0,
        "page_size": 25,
    }
    response = client.request(
        "GET",
        "/Composants",
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200

