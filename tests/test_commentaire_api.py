# coding: utf-8

from fastapi.testclient import TestClient


from gestocks_server.models.commentaire import Commentaire  # noqa: F401


def test_comment_by_id_id_get(client: TestClient):
    """Test case for comment_by_id_id_get

    
    """

    headers = {
    }
    response = client.request(
        "GET",
        "/comment/byId/{id}/".format(id=56),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    #assert response.status_code == 200

