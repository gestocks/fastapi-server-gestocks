# coding: utf-8

from typing import Dict, List  # noqa: F401
from fastapi import (  # noqa: F401
    APIRouter,
    Body,
    Cookie,
    Depends,
    Form,
    Header,
    Path,
    Query,
    Response,
    Security,
    status,
    HTTPException
)

from gestocks_server.models.extra_models import TokenModel  # noqa: F401
from gestocks_server.models.contenu_emplacement import ContenuEmplacement, ContenuEmplacementContenuInner
from gestocks_server.models.emplacement import Emplacement
from gestocks_server.models.declaration_composant import DeclarationComposant
from gestocks_server.security_api import get_token_oauthToken
from gestocks_server.database import pool
from psycopg.rows import class_row, dict_row

router = APIRouter()


@router.get(
    "/Emplacement/byId/{id}",
    responses={
        200: {"model": Emplacement, "description": "OK"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Emplacement"],
    summary="Accesseur d&#39;un emplacement par son Id",
    response_model_by_alias=True,
)
async def emplacement_by_id_id_get(
    id: int = Path(description="id de l&#39;emplacement.", ge=0),
) -> Emplacement:
    """Obtient un emplacements par son Id"""
    async with pool.connection() as conn, conn.cursor(row_factory=class_row(Emplacement)) as cur:
        await cur.execute('SELECT id, nom FROM emplacement WHERE id=%s', [id])
        res = await cur.fetchall()
        if len(res) == 0:
            raise HTTPException(404, f"There is no emplacement {id} !")
        if len(res) > 1:
            raise ValueError
        return res[0]


@router.patch(
    "/Emplacement/byId/{id}",
    responses={
        200: {"model": Emplacement, "description": "OK"},
        401: {"description": "API key is missing or invalid"},
        403: {"description": "User forbiden for operation"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Emplacement"],
    summary="Update des emplacements",
    response_model_by_alias=True,
)
async def emplacement_by_id_id_patch(
    id: int = Path(description="id de l&#39;emplacement.", ge=0),
    emplacement: Emplacement = Body(None, description=""),
    token_oauthToken: TokenModel = Security(
        get_token_oauthToken, scopes=["GESTION_MAGASIN"]
    ),
) -> Emplacement:
    """Update un emplacements par son Id"""
    if id != emplacement.id:
        raise HTTPException(400, 'Incohérence dans la requet')
    async with pool.connection() as conn, conn.cursor(row_factory=class_row(Emplacement)) as cur:
        await cur.execute('UPDATE emplacement SET nom=%(nom)s WHERE id=%(id)s', {'nom': emplacement.nom, 'id': emplacement.id})
        res = await cur.fetchall()
        if len(res) == 0:
            raise HTTPException(404, f"There is no emplacement {id} !")
        if len(res) > 1:
            raise ValueError
        return res[0]


@router.get(
    "/Emplacement/contenu/{idEmplacement}",
    responses={
        200: {"model": ContenuEmplacement, "description": "OK"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Inventaire", "Emplacement"],
    summary="Getteur du contenu d&#39;un emplacement.",
    response_model_by_alias=True,
)
async def emplacement_contenu_id_emplacement_get(
    page_number: int = Header(0, description="Numero de la page voulue", ge=0),
    page_size: int = Header(
        25, description="Taille des pages, 0 pour page infini", ge=0),
    idEmplacement: int = Path(description="id de l&#39;emplacement", ge=0),
) -> ContenuEmplacement:
    """Contenu de l&#39;emplacement scpécifié."""
    async with pool.connection() as conn, conn.transaction():
        async with conn.cursor(row_factory=class_row(Emplacement)) as cur:
            await cur.execute('SELECT id, nom FROM emplacement WHERE id=%s', (idEmplacement, ))
            empls = await cur.fetchall()
            if len(empls) == 0:
                raise HTTPException(404, f"There is no emplacement {id} !")
            if len(empls) > 1:
                raise ValueError
            empl = empls[0]
        async with conn.cursor(row_factory=dict_row) as cur:
            await cur.execute('SELECT d.id as id, d.nom as nom, COUNT(*) as nombre FROM definition_composant AS d JOIN instance_composant AS i ON i.id_definition=d.id WHERE i.id_emplacement=%(id)s GROUP BY (d.id, d.nom ) ORDER BY nombre LIMIT %(limite)s OFFSET %(offset)s;',
                              {'id': idEmplacement, 'limite': page_size, 'offset': page_number*page_size})
            res = await cur.fetchall()
        return ContenuEmplacement(emplacement=empl, contenu=[ContenuEmplacementContenuInner(nombre_disponible=elem['nombre'], definition_composant=DeclarationComposant(id=elem['id'], nom=elem["nom"])) for elem in res])


@router.post(
    "/Emplacement",
    responses={
        200: {"model": Emplacement, "description": "Ajout OK, renvoie le nouvel emplacement."},
        400: {"model": str, "description": "Inpute data used not usable"},
        401: {"description": "API key is missing or invalid"},
        403: {"description": "User forbiden for operation"},
    },
    tags=["Emplacement"],
    summary="Création d&#39;un nouvel emplacement.",
    response_model_by_alias=True,
)
async def emplacement_post(
    emplacement: Emplacement = Body(None, description=""),
    token_oauthToken: TokenModel = Security(
        get_token_oauthToken, scopes=["GESTION_MAGASIN"]
    ),
) -> Emplacement:
    """Ajout d&#39;un nouvel emplacement."""
    async with pool.connection() as conn, conn.cursor(row_factory=class_row(Emplacement)) as cur:
        await cur.execute('INSERT INTO emplacement(id, nom) VALUES(DEFAULT, %s) RETURNING id, nom;', [emplacement.nom])
        res = await cur.fetchall()
        if len(res) == 0:
            raise HTTPException(404, f"There is no emplacement {id} !")
        if len(res) > 1:
            raise ValueError
        return res[0]


@router.get(
    "/Emplacements",
    responses={
        200: {"model": List[Emplacement], "description": "OK"},
    },
    tags=["Emplacement"],
    summary="Accesseur pour la liste des composants.",
    response_model_by_alias=True,
)
async def emplacements_get(
    page_number: int = Header(0, description="Numero de la page voulue", ge=0),
    page_size: int = Header(
        25, description="Taille des pages, 0 pour page infini", ge=0),
) -> List[Emplacement]:
    """Liste de tous les emplacements de la base de donnée"""
    async with pool.connection() as conn, conn.cursor(row_factory=class_row(Emplacement)) as cur:
        await cur.execute('SELECT id, nom FROM emplacement LIMIT %(limite)s OFFSET %(offset)s;', {'limite': page_size, 'offset': page_number*page_size})
        res = await cur.fetchall()
        return res
