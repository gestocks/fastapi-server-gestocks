# coding: utf-8

from typing import Dict, List  # noqa: F401

from fastapi.responses import StreamingResponse as file

from fastapi import (  # noqa: F401
    APIRouter,
    Body,
    Cookie,
    Depends,
    Form,
    Header,
    Path,
    Query,
    Response,
    Security,
    status,
    HTTPException,
)

from fastapi.responses import StreamingResponse

from gestocks_server.models.extra_models import TokenModel  # noqa: F401
from gestocks_server.security_api import get_token_oauthToken
from gestocks_server.database import pool
import io
from PIL import Image, ImageOps
import psycopg as pg
from psycopg.rows import dict_row

low_resolution_size = (10, 10)
mime_png = 'image/png'

router = APIRouter()


async def add_img(png: bytes, cur: pg.AsyncCursor[dict[str, int]]) -> int:
    full_img = Image.open(io.BytesIO(png))
    low_img = ImageOps.contain(full_img, low_resolution_size)
    buffer_full = io.BytesIO()
    buffer_low = io.BytesIO()
    full_img.save(buffer_full, 'png')
    low_img.save(buffer_low, 'png')
    b_full = buffer_full.getvalue()
    b_low = buffer_low.getvalue()
    await cur.execute("INSERT INTO images(id, large, small) VALUES (DEFAULT, %(full)s, %(low)s) RETURNING id", {'full': b_full, 'low': b_low})
    res = await cur.fetchone()
    if res is None:
        raise TypeError
    img_id = res['id']
    return img_id


@router.get(
    "/Composant/byId/{id}/image",
    responses={
        200: {"content": {"image/*": {}}, "description": "Ok"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Composants", "Image"],
    response_model_by_alias=True,
)
async def image_composant_by_id_id_image_get(
    id: int = Path(description="id du composant", ge=0),
) -> Response:
    """Obtient l&#39;image d&#39;un composant par son Id"""
    async with pool.connection() as conn, conn.cursor(row_factory=dict_row) as cur:
        await cur.execute("SELECT i.large AS png FROM images AS i JOIN definition_composant AS d on d.id_image = i.id WHERE d.id=%(comp)s;", {'comp': id})
        res = await cur.fetchone()
        if res is None:
            raise HTTPException(404, "Pas d'image pour le composant")
        b = bytes(res['png'])
        return Response(content=b, media_type=mime_png)


@router.post(
    "/Composant/byId/{id}/image",
    responses={
        200: {"description": "Ok"},
        400: {"model": str, "description": "Inpute data used not usable"},
        404: {"model": str, "description": "Id used is not in the database"},
        401: {"description": "API key is missing or invalid"},
        403: {"description": "User forbiden for operation"},
    },
    tags=["Composants", "Image"],
    response_model_by_alias=True,
)
async def image_composant_by_id_id_image_post(
    id: int = Path(description="id du composant", ge=0),
    body: bytes = Body(None, description="", media_type=mime_png),
    token_oauthToken: TokenModel = Security(
        get_token_oauthToken, scopes=["GESTION_STOCKS"]
    ),
) -> None:
    """Depose une photo pour le composant"""
    async with pool.connection() as conn, conn.transaction(), conn.cursor(row_factory=dict_row) as cur:
        img_id = await add_img(body, cur)
        await cur.execute("UPDATE definition_composant SET id_image=%(img)s WHERE id=%(comp)s;", {'img': img_id, 'comp': id})
    return


@router.get(
    "/Composant/byId/{id}/low_image",
    responses={
        200: {"content": {"image/*": {}}, "description": "Ok"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Composants", "Image"],
    response_model_by_alias=True,
)
async def image_composant_by_id_id_low_image_get(
    id: int = Path(description="id du composant", ge=0),
) -> Response:
    """Obtient l&#39;image basse résolution d&#39;un composant par son Id"""
    async with pool.connection() as conn, conn.cursor(row_factory=dict_row) as cur:
        await cur.execute("SELECT i.small AS png FROM images AS i JOIN definition_composant AS d on d.id_image = i.id WHERE d.id=%(comp)s;", {'comp': id})
        res = await cur.fetchone()
        if res is None:
            raise HTTPException(404, "Pas d'image pour le composant")
        b = bytes(res['png'])
        return Response(content=b, media_type=mime_png)


@router.get(
    "/Instance/byId/{id}/image",
    responses={
        200: {"content": {"image/*": {}}, "description": "Ok"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Instances", "Image"],
    response_model_by_alias=True,
)
async def image_instance_by_id_id_image_get(
    id: int = Path(description="id de l&#39;instance", ge=0),
) -> Response:
    """Obtient l&#39;image d&#39;une instance par son Id"""
    async with pool.connection() as conn, conn.cursor(row_factory=dict_row) as cur:
        await cur.execute("SELECT i.large AS png FROM images AS i JOIN instance_composant AS d on d.id_image = i.id WHERE d.id=%(comp)s;", {'comp': id})
        res = await cur.fetchone()
        if res is None:
            raise HTTPException(404, "Pas d'image pour le composant")
        b = bytes(res['png'])
        return Response(content=b, media_type=mime_png)


@router.post(
    "/Instance/byId/{id}/image",
    responses={
        200: {"description": "Ok"},
        400: {"model": str, "description": "Inpute data used not usable"},
        404: {"model": str, "description": "Id used is not in the database"},
        401: {"description": "API key is missing or invalid"},
        403: {"description": "User forbiden for operation"},
    },
    tags=["Instances", "Image"],
    response_model_by_alias=True,
)
async def image_instance_by_id_id_image_post(
    id: int = Path(description="id de l&#39;instance", ge=0),
    body: bytes = Body(None, description="", media_type=mime_png),
    token_oauthToken: TokenModel = Security(
        get_token_oauthToken, scopes=["GESTION_STOCKS"]
    ),
) -> None:
    """Depose une photo pour l&#39;instance"""
    async with pool.connection() as conn, conn.transaction(), conn.cursor(row_factory=dict_row) as cur:
        img_id = await add_img(body, cur)
        await cur.execute("UPDATE instance_composant SET id_image=%(img)s WHERE id=%(comp)s;", {'img': img_id, 'comp': id})
    return


@router.get(
    "/Instance/byId/{id}/low_image",
    responses={
        200: {"content": {"image/*": {}}, "description": "Ok"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Instances", "Image"],
    response_model_by_alias=True,
)
async def image_instance_by_id_id_low_image_get(
    id: int = Path(description="id de l&#39;instance", ge=0),
) -> Response:
    """Obtient l&#39;image basse résolutione d&#39;un instance par son Id"""
    async with pool.connection() as conn, conn.cursor(row_factory=dict_row) as cur:
        await cur.execute("SELECT i.small AS png FROM images AS i JOIN instance_composant AS d on d.id_image = i.id WHERE d.id=%(comp)s;", {'comp': id})
        res = await cur.fetchone()
        if res is None:
            raise HTTPException(404, "Pas d'image pour le composant")
        b = bytes(res['png'])
        return Response(content=b, media_type=mime_png)
