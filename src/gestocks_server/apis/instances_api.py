# coding: utf-8

from typing import Dict, List  # noqa: F401
import datetime
from fastapi.responses import StreamingResponse as file

from fastapi import (  # noqa: F401
    APIRouter,
    Body,
    Cookie,
    Depends,
    Form,
    Header,
    Path,
    Query,
    Response,
    Security,
    status,
    HTTPException
)

from gestocks_server.models.extra_models import TokenModel  # noqa: F401
from gestocks_server.models.commentaire import Commentaire, CommentaireReference
from gestocks_server.models.instance_composant import InstanceComposant
from gestocks_server.security_api import get_token_oauthToken
from gestocks_server.database import pool
from gestocks_server.apis.image_api import image_instance_by_id_id_image_get, image_instance_by_id_id_image_post, image_instance_by_id_id_low_image_get, mime_png
from psycopg.rows import class_row, dict_row

router = APIRouter()


@router.get(
    "/Instance/byId/{id}/comments",
    responses={
        200: {"model": List[int], "description": "OK"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Instances"],
    summary="Accesseur à la liste des commentaires",
    response_model_by_alias=True,
)
async def instance_by_id_id_comments_get(
    id: int = Path(description="id de la définition de l&#39;instance.", ge=0),
) -> List[int]:
    """Obtient une list des commentaire d&#39;une instance"""
    async with pool.connection() as conn, conn.cursor(row_factory=dict_row) as cur:
        await cur.execute('SELECT id FROM commentaire_instance WHERE def=%s', [id])
        res = await cur.fetchall()
        return [int(x['id']) for x in res]


@router.post(
    "/Instance/byId/{id}/comments",
    responses={
        200: {"model": Commentaire, "description": "OK"},
        404: {"model": str, "description": "Id used is not in the database"},
        401: {"description": "API key is missing or invalid"},
        403: {"description": "User forbiden for operation"},
    },
    tags=["Instances"],
    summary="Création d&#39;un commentaire de instance",
    response_model_by_alias=True,
)
async def instance_by_id_id_comments_post(
    id: int = Path(description="id de la définition de instance.", ge=0),
    body: str = Body(None, description="Texte du commentaire"),
    token_oauthToken: TokenModel = Security(
        get_token_oauthToken, scopes=["USER"]
    ),
) -> Commentaire:
    """Créé un nouveaux commentaire sur ce instance"""
    async with pool.connection() as conn, conn.transaction():
        async with conn.cursor(row_factory=dict_row) as cur:
            await cur.execute("INSERT INTO commentaire_instance(id, contenu, auteur, timestamp, def) VALUES(DEFAULT, %(contenu)s, %(auteur)s, DEFAULT, %(ref)s) RETURNING id, contenu, auteur, timestamp, ref;", {'contenu': body, 'auteur': token_oauthToken.user_id, 'ref': id})
            res = await cur.fetchone()
            if res is None:
                raise TypeError('No return')
            auteur_id = res['auteur']
        async with conn.cursor(row_factory=dict_row) as cur:
            await cur.execute("SELECT nom FROM users WHERE id=%s", [auteur_id])
            res_nom = await cur.fetchone()
            if res_nom is None:
                raise TypeError('No return')
            auteur = res_nom['nom']
    time_str: datetime.datetime = res["timestamp"]
    return Commentaire(auteur=auteur, contenu=res["contenu"], id=res['id'], reference=CommentaireReference(ref_type="Instance", ref_id=res["def"]), timestamp=time_str.isoformat())


@router.delete(
    "/Instance/byId/{id}",
    responses={
        200: {"model": InstanceComposant, "description": "OK"},
        401: {"description": "API key is missing or invalid"},
        403: {"description": "User forbiden for operation"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Instances"],
    summary="Destructeur d&#39;une instance par son Id.",
    response_model_by_alias=True,
)
async def instance_by_id_id_delete(
    id: int = Path(description="id de l&#39;instance à supprimée.", ge=0),
    token_oauthToken: TokenModel = Security(
        get_token_oauthToken, scopes=["GESTION_STOCKS"]
    ),
) -> InstanceComposant:
    """Supprime une instance de composant"""
    async with pool.connection() as conn, conn.transaction(), conn.cursor(row_factory=class_row(InstanceComposant)) as cur:
        await cur.execute("DELETE FROM instance_composant WHERE id=%s RETURNING id, id_emplacement, id_definition;", [id])
        res = await cur.fetchall()
        if len(res) == 0:
            raise HTTPException(404, f"There is no instance {id} !")
        if len(res) > 1:
            raise ValueError
    return res[0]


@router.get(
    "/Instance/byId/{id}",
    responses={
        200: {"model": InstanceComposant, "description": "OK"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Instances"],
    summary="Accesseur d&#39;une instance par son Id",
    response_model_by_alias=True,
)
async def instance_by_id_id_get(
    id: int = Path(description="id de l&#39;instance.", ge=0),
) -> InstanceComposant:
    """Obtient une instance de composant par son Id"""
    async with pool.connection() as conn, conn.cursor(row_factory=class_row(InstanceComposant)) as cur:
        await cur.execute('SELECT id, id_definition, id_emplacement FROM instance_composant WHERE id=%s', [id])
        res = await cur.fetchall()
        if len(res) == 0:
            raise HTTPException(404, f"There is no instance {id} !")
        if len(res) > 1:
            raise ValueError
        return res[0]


@router.get(
    "/Instance/byId/{id}/image",
    responses={
        200: {"content": {"image/*": {}}, "description": "Ok"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Instances", "Image"],
    response_model_by_alias=True,
)
async def instance_by_id_id_image_get(
    id: int = Path(description="id de l&#39;instance", ge=0),
) -> Response:
    """Obtient l&#39;image d&#39;une instance par son Id"""
    return await image_instance_by_id_id_image_get(id)


@router.post(
    "/Instance/byId/{id}/image",
    responses={
        200: {"description": "Ok"},
        400: {"model": str, "description": "Inpute data used not usable"},
        404: {"model": str, "description": "Id used is not in the database"},
        401: {"description": "API key is missing or invalid"},
        403: {"description": "User forbiden for operation"},
    },
    tags=["Instances", "Image"],
    response_model_by_alias=True,
)
async def instance_by_id_id_image_post(
    id: int = Path(description="id de l&#39;instance", ge=0),
    body: bytes = Body(None, description="", media_type=mime_png),
    token_oauthToken: TokenModel = Security(
        get_token_oauthToken, scopes=["GESTION_STOCKS"]
    ),
) -> None:
    """Depose une photo pour l&#39;instance"""
    await image_instance_by_id_id_image_post(id, body, token_oauthToken)
    return


@router.get(
    "/Instance/byId/{id}/low_image",
    responses={
        200: {"content": {"image/*": {}}, "description": "Ok"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Instances", "Image"],
    response_model_by_alias=True,
)
async def instance_by_id_id_low_image_get(
    id: int = Path(description="id de l&#39;instance", ge=0),
) -> Response:
    """Obtient l&#39;image basse résolutione d&#39;un instance par son Id"""
    return await image_instance_by_id_id_low_image_get(id)


@router.patch(
    "/Instance/deplacement/{id_instance}/vers/{id_emplacement}",
    responses={
        200: {"model": InstanceComposant, "description": "OK"},
        401: {"description": "API key is missing or invalid"},
        403: {"description": "User forbiden for operation"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Instances"],
    summary="Mutateur de l&#39;emplacement d&#39;une instance.",
    response_model_by_alias=True,
)
async def instance_deplacement_id_instance_vers_id_emplacement_patch(
    id_instance: int = Path(description="id de l&#39;instance.", ge=0),
    id_emplacement: int = Path(description="id de l&#39;emplacement.", ge=0),
    token_oauthToken: TokenModel = Security(
        get_token_oauthToken, scopes=["GESTION_USERS"]
    ),
) -> InstanceComposant:
    """Déplace une instance dans un nouvelle emplacement."""
    async with pool.connection() as conn, conn.cursor(row_factory=class_row(InstanceComposant)) as cur:
        await cur.execute('UPDATE definition_composant SET id_emplacement=%(emplacement)s WHERE id=%(id)s RETURNING id, id_composant, id_emplacement;', {'id': id_instance, 'emplacement': id_emplacement})
        res = await cur.fetchall()
        if len(res) == 0:
            raise HTTPException(404, f"There is no instance {id} !")
        if len(res) > 1:
            raise ValueError
        return res[0]


@router.post(
    "/Instance",
    responses={
        200: {"model": InstanceComposant, "description": "Ajout OK, renvoie la nouvelle instance de composant."},
        400: {"model": str, "description": "Inpute data used not usable"},
        401: {"description": "API key is missing or invalid"},
        403: {"description": "User forbiden for operation"},
    },
    tags=["Instances"],
    summary="Création d&#39;une nouvelle instance de composant.",
    response_model_by_alias=True,
)
async def instance_post(
    instance_composant: InstanceComposant = Body(None, description=""),
    token_oauthToken: TokenModel = Security(
        get_token_oauthToken, scopes=["GESTION_STOCKS"]
    ),
) -> InstanceComposant:
    """Ajout d&#39;une nouvelle instance de composant."""
    async with pool.connection() as conn, conn.cursor(row_factory=class_row(InstanceComposant)) as cur:
        await cur.execute('INSERT INTO instance_composant(id, id_emplacement, id_definition) VALUES (DEFAULT, %(emplacement)s, %(definition)s) RETURNING id, id_emplacement, id_definition;', {'emplacement': instance_composant.id_emplacement, 'definition': instance_composant.id_definition})
        res = await cur.fetchall()
        return res[0]


@router.get(
    "/Instances",
    responses={
        200: {"model": List[InstanceComposant], "description": "OK"},
    },
    tags=["Instances"],
    summary="Accesseur à la liste des instances.",
    response_model_by_alias=True,
)
async def instances_get(
    page_number: int = Header(0, description="Numero de la page voulue", ge=0),
    page_size: int = Header(
        25, description="Taille des pages, 0 pour page infini", ge=0),
) -> List[InstanceComposant]:
    """Liste de tous les instance de composant de la base de donnée"""
    async with pool.connection() as conn, conn.cursor(row_factory=class_row(InstanceComposant)) as cur:
        await cur.execute('SELECT id, id_definition, id_emplacement FROM instance_composant LIMIT %(limite)s OFFSET %(offset)s;', {'limite': page_size, 'offset': page_number*page_size})
        res = await cur.fetchall()
        return res
