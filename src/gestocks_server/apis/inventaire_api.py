# coding: utf-8

from typing import Dict, List  # noqa: F401

from fastapi import (  # noqa: F401
    APIRouter,
    Body,
    Cookie,
    Depends,
    Form,
    Header,
    Path,
    Query,
    Response,
    Security,
    status,
)

from gestocks_server.models.extra_models import TokenModel  # noqa: F401
from gestocks_server.models.contenu_emplacement import ContenuEmplacement
from gestocks_server.models.repartition_instance import RepartitionInstance
from gestocks_server.database import pool

from .composants_api import composant_repartition_id_definition_get
from .emplacement_api import emplacement_contenu_id_emplacement_get

router = APIRouter()


@router.get(
    "/Composant/repartition/{idDefinition}",
    responses={
        200: {"model": RepartitionInstance, "description": "OK"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Inventaire", "Composants"],
    summary="Recherche de la position d&#39;un composant.",
    response_model_by_alias=True,
)
async def inventory_repartition_id_definition_get(
    page_number: int = Header(0, description="Numero de la page voulue", ge=0),
    page_size: int = Header(
        25, description="Taille des pages, 0 pour page infini", ge=0),
    idDefinition: int = Path(description="id de la definition.", ge=0),
) -> RepartitionInstance:
    """Répartition du composant."""
    return await composant_repartition_id_definition_get(page_number=page_number, page_size=page_size, idDefinition=idDefinition)


@router.get(
    "/Emplacement/contenu/{idEmplacement}",
    responses={
        200: {"model": ContenuEmplacement, "description": "OK"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Inventaire", "Emplacement"],
    summary="Getteur du contenu d&#39;un emplacement.",
    response_model_by_alias=True,
)
async def inventory_contenu_id_emplacement_get(
    page_number: int = Header(0, description="Numero de la page voulue", ge=0),
    page_size: int = Header(
        25, description="Taille des pages, 0 pour page infini", ge=0),
    idEmplacement: int = Path(description="id de l&#39;emplacement", ge=0),
) -> ContenuEmplacement:
    """Contenu de l&#39;emplacement scpécifié."""
    return await emplacement_contenu_id_emplacement_get(page_number=page_number, page_size=page_size, idEmplacement=idEmplacement)
