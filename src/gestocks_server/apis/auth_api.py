# coding: utf-8

from fastapi import HTTPException
from hashlib import scrypt
from gestocks_server.password_config import scrypt_config, tokken_generator_base
from gestocks_server.database import pool
from typing import Dict, List  # noqa: F401

from fastapi import (  # noqa: F401
    APIRouter,
    Body,
    Cookie,
    Depends,
    Form,
    Header,
    Path,
    Query,
    Response,
    Security,
    status,
)

from gestocks_server.models.extra_models import TokenModel  # noqa: F401
from gestocks_server.models.login_post200_response import LoginPost200Response
from gestocks_server.security_api import get_token_oauthToken

router = APIRouter()


tokken_gen = tokken_generator_base(256)


@router.post(
    "/login",
    responses={
        200: {"model": LoginPost200Response, "description": "Authentification réussi"},
        401: {"model": str, "description": "Authentification raté"},
    },
    tags=["Auth"],
    response_model_by_alias=True,
)
async def login_post(
    username: str = Form(None, description="Nom d&#39;utilisateur"),
    password: str = Form(None, description="Mot de passe"),
    grant_type: str = Form(None, description=""),
    scope: str = Form(
        None, description="Liste des scope demandés, séparé par des espaces"),
) -> LoginPost200Response:
    """Generation d&#39;un tokken de session"""
    async with pool.connection() as conn, conn.cursor() as cur:
        await cur.execute("SELECT id, salt, psw_hash FROM users WHERE nom = %s;", [username])
        res = await cur.fetchone()
        if res is None:
            raise HTTPException(400, "Mauvais login")
        id: int = res[0]
        salt: bytes = res[1]
        psw: bytes = res[2]
        if scrypt(password.encode(), salt=salt, **scrypt_config) == psw:
            tokken = next(tokken_gen)
            await cur.execute("INSERT INTO session_tokken(tokken, users) VALUES(%s, %s);", [tokken, id])
            return LoginPost200Response(access_token=tokken, token_type="bearer")
    raise HTTPException(400, "Mauvais login")


@router.get(
    "/logout",
    responses={
        200: {"description": "Déauthentification réussi"},
    },
    tags=["Auth"],
    response_model_by_alias=True,
)
async def logout_get(
    token_oauthToken: TokenModel = Security(
        get_token_oauthToken, scopes=["User"]
    ),
) -> None:
    """Suppression d&#39;un tokken de session"""
    async with pool.connection() as conn, conn.cursor() as cur:
        await cur.execute("UPDATE session_tokken SET revoked=true WHERE tokken=%s AND users=%s", [token_oauthToken.tokken, token_oauthToken.user_id])
    return
