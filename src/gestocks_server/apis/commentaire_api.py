# coding: utf-8

from typing import Dict, List  # noqa: F401

from fastapi import (  # noqa: F401
    APIRouter,
    Body,
    Cookie,
    Depends,
    Form,
    Header,
    Path,
    Query,
    Response,
    Security,
    status,
    HTTPException
)

from gestocks_server.models.extra_models import TokenModel  # noqa: F401
from gestocks_server.models.commentaire import Commentaire, CommentaireReference

from psycopg.rows import class_row, dict_row
from gestocks_server.database import pool

router = APIRouter()


@router.get(
    "/comment/byId/{id}/",
    responses={
        200: {"model": Commentaire, "description": "Commentaire recherché"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Commentaire"],
    response_model_by_alias=True,
)
async def comment_by_id_id_get(
    id: int = Path(description="", ge=1),
) -> Commentaire:
    """Obtient les details d&#39;un commentaire"""
    async with pool.connection() as conn, conn.cursor(row_factory=dict_row) as cur:
        await cur.execute('SELECT c.id AS id, contenu, nom, timestamp, def FROM commentaire_definition AS c JOIN users ON auteur=users.id WHERE c.id=%s;', (id,))
        ress = await cur.fetchall()
        if len(ress) == 1:
            res = ress[0]
            return Commentaire(id=res['id'], contenu=res['contenu'], auteur=res['nom'], timestamp=str(res['timestamp']), reference=CommentaireReference(ref_type='Composant', ref_id=res["def"]))
        await cur.execute('SELECT c.id AS id, contenu, nom, timestamp, def FROM commentaire_instance AS c JOIN users ON auteur=users.id WHERE c.id=%s;', (id,))
        ress = await cur.fetchall()
        if len(ress) == 1:
            res = ress[0]
            return Commentaire(id=res['id'], contenu=res['contenu'], auteur=res['nom'], timestamp=str(res['timestamp']), reference=CommentaireReference(ref_type='Instance', ref_id=res["def"]))
        raise HTTPException(404, "Commentaire not found")
