# coding: utf-8

from typing import Dict, List  # noqa: F401
import datetime
from fastapi.responses import StreamingResponse as file

from fastapi import (  # noqa: F401
    APIRouter,
    Body,
    Cookie,
    Depends,
    Form,
    Header,
    Path,
    Query,
    Response,
    Security,
    status,
    HTTPException
)

from gestocks_server.models.extra_models import TokenModel  # noqa: F401
from gestocks_server.models.commentaire import Commentaire, CommentaireReference
from gestocks_server.models.declaration_composant import DeclarationComposant
from gestocks_server.models.repartition_instance import RepartitionInstance, RepartitionInstanceContenuInner
from gestocks_server.models.emplacement import Emplacement
from gestocks_server.security_api import get_token_oauthToken
from gestocks_server.database import pool
from gestocks_server.apis.image_api import image_composant_by_id_id_image_get, image_composant_by_id_id_image_post, image_composant_by_id_id_low_image_get, mime_png
from psycopg.rows import class_row, dict_row
router = APIRouter()


@router.get(
    "/Composant/byId/{id}/comments",
    responses={
        200: {"model": List[int], "description": "OK"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Composants"],
    summary="Accesseur à la liste des commentaires",
    response_model_by_alias=True,
)
async def composant_by_id_id_comments_get(
    id: int = Path(description="id de la définition de composant.", ge=0),
) -> List[int]:
    """Obtient une list des commentaire du composant"""
    async with pool.connection() as conn, conn.cursor(row_factory=dict_row) as cur:
        await cur.execute('SELECT id FROM commentaire_definition WHERE def=%s', [id])
        res = await cur.fetchall()
        return [int(x['id']) for x in res]


@router.post(
    "/Composant/byId/{id}/comments",
    responses={
        200: {"model": Commentaire, "description": "OK"},
        404: {"model": str, "description": "Id used is not in the database"},
        401: {"description": "API key is missing or invalid"},
        403: {"description": "User forbiden for operation"},
    },
    tags=["Composants"],
    summary="Création d&#39;un commentaire de composant",
    response_model_by_alias=True,
)
async def composant_by_id_id_comments_post(
    id: int = Path(description="id de la définition de composant.", ge=0),
    body: str = Body(None, description="Texte du commentaire"),
    token_oauthToken: TokenModel = Security(
        get_token_oauthToken, scopes=["USER"]
    ),
) -> Commentaire:
    """Créé un nouveaux commentaire sur ce composant"""
    async with pool.connection() as conn, conn.transaction():
        async with conn.cursor(row_factory=dict_row) as cur:
            await cur.execute("INSERT INTO commentaire_definition(id, contenu, auteur, timestamp, def) VALUES(DEFAULT, %(contenu)s, %(auteur)s, DEFAULT, %(ref)s) RETURNING id, contenu, auteur, timestamp, def;", {'contenu': body, 'auteur': token_oauthToken.user_id, 'ref': id})
            res = await cur.fetchone()
            if res is None:
                raise TypeError('No return')
            auteur_id = res['auteur']
        async with conn.cursor(row_factory=dict_row) as cur:
            await cur.execute("SELECT nom FROM users WHERE id=%s", [auteur_id])
            res_nom = await cur.fetchone()
            if res_nom is None:
                raise TypeError('No return')
            auteur = res_nom['nom']
    time_str: datetime.datetime = res["timestamp"]
    return Commentaire(auteur=auteur, contenu=res["contenu"], id=res['id'], reference=CommentaireReference(ref_type="Composant", ref_id=res["def"]), timestamp=time_str.isoformat())


@router.get(
    "/Composant/byId/{id}",
    responses={
        200: {"model": DeclarationComposant, "description": "OK"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Composants"],
    summary="Accesseur d&#39;une définition de composant par son Id",
    response_model_by_alias=True,
)
async def composant_by_id_id_get(
    id: int = Path(description="id de la définition de composant.", ge=0),
) -> DeclarationComposant:
    """Obtient une définition de composant par son Id"""
    async with pool.connection() as conn, conn.cursor(row_factory=class_row(DeclarationComposant)) as cur:
        await cur.execute('SELECT id, nom FROM definition_composant WHERE id=%s', [id])
        res = await cur.fetchall()
        if len(res) == 0:
            raise HTTPException(404, f"There is no emplacement {id} !")
        if len(res) > 1:
            raise ValueError
        return res[0]


@router.get(
    "/Composant/byId/{id}/image",
    responses={
        200: {"content": {"image/*": {}}, "description": "Ok"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Composants", "Image"],
    response_model_by_alias=True,
)
async def composant_by_id_id_image_get(
    id: int = Path(description="id du composant", ge=0),
) -> Response:
    """Obtient l&#39;image d&#39;un composant par son Id"""
    return await image_composant_by_id_id_image_get(id)


@router.post(
    "/Composant/byId/{id}/image",
    responses={
        200: {"description": "Ok"},
        400: {"model": str, "description": "Inpute data used not usable"},
        404: {"model": str, "description": "Id used is not in the database"},
        401: {"description": "API key is missing or invalid"},
        403: {"description": "User forbiden for operation"},
    },
    tags=["Composants", "Image"],
    response_model_by_alias=True,
)
async def composant_by_id_id_image_post(
    id: int = Path(description="id du composant", ge=0),
    body: bytes = Body(None, description="", media_type=mime_png),
    token_oauthToken: TokenModel = Security(
        get_token_oauthToken, scopes=["GESTION_STOCKS"]
    ),
) -> None:
    """Depose une photo pour le composant"""
    await image_composant_by_id_id_image_post(id, body, token_oauthToken)
    return


@router.get(
    "/Composant/byId/{id}/low_image",
    responses={
        200: {"content": {"image/*": {}}, "description": "Ok"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Composants", "Image"],
    response_model_by_alias=True,
)
async def composant_by_id_id_low_image_get(
    id: int = Path(description="id du composant", ge=0),
) -> Response:
    """Obtient l&#39;image basse résolution d&#39;un composant par son Id"""
    return await image_composant_by_id_id_low_image_get(id)


@router.post(
    "/Composant",
    responses={
        200: {"model": DeclarationComposant, "description": "Ajout OK, renvoie la nouvelle définition de composant"},
        400: {"model": str, "description": "Inpute data used not usable"},
        401: {"description": "API key is missing or invalid"},
        403: {"description": "User forbiden for operation"},
    },
    tags=["Composants"],
    summary="Création d&#39;un nouveaux type de composants.",
    response_model_by_alias=True,
)
async def composant_post(
    declaration_composant: DeclarationComposant = Body(None, description=""),
    token_oauthToken: TokenModel = Security(
        get_token_oauthToken, scopes=["GESTION_STOCKS"]
    ),
) -> DeclarationComposant:
    """Ajout d&#39;une nouvelle définition de composants."""
    async with pool.connection() as conn, conn.cursor(row_factory=class_row(DeclarationComposant)) as cur:
        await cur.execute('INSERT INTO definition_composant(id, nom) VALUES(DEFAULT, %s) RETURNING id, nom;', [declaration_composant.nom])
        res = await cur.fetchall()
        return res[0]


@router.get(
    "/Composant/repartition/{idDefinition}",
    responses={
        200: {"model": RepartitionInstance, "description": "OK"},
        404: {"model": str, "description": "Id used is not in the database"},
    },
    tags=["Inventaire", "Composants"],
    summary="Recherche de la position d&#39;un composant.",
    response_model_by_alias=True,
)
async def composant_repartition_id_definition_get(
    page_number: int = Header(0, description="Numero de la page voulue", ge=0),
    page_size: int = Header(
        25, description="Taille des pages, 0 pour page infini", ge=0),
    idDefinition: int = Path(description="id de la definition.", ge=0),
) -> RepartitionInstance:
    """Répartition du composant."""
    async with pool.connection() as conn, conn.transaction():
        async with conn.cursor(row_factory=class_row(DeclarationComposant)) as cur:
            await cur.execute('SELECT id, nom FROM definition_composant WHERE id=%s', (idDefinition, ))
            insts = await cur.fetchall()
            if len(insts) == 0:
                raise HTTPException(404, f"There is no definition {idDefinition} !")
            if len(insts) > 1:
                raise ValueError
            inst = insts[0]
        async with conn.cursor(row_factory=dict_row) as cur:
            await cur.execute('SELECT e.id, e.nom, COUNT(*) as nombre FROM emplacement AS e JOIN instance_composant AS i ON i.id_emplacement=e.id WHERE i.id_definition=%(id)s GROUP BY (e.id, e.nom ) ORDER BY nombre LIMIT %(limite)s OFFSET %(offset)s;',
                              {'id': idDefinition, 'limite': page_size, 'offset': page_number*page_size})
            res = await cur.fetchall()
        return RepartitionInstance(instance=inst, contenu=[RepartitionInstanceContenuInner(nombre_disponible=elem['nombre'], emplacement=Emplacement(id=elem['id'], nom=elem["nom"])) for elem in res])


@router.get(
    "/Composants",
    responses={
        200: {"model": List[DeclarationComposant], "description": "OK"},
    },
    tags=["Composants"],
    summary="Accesseur pour la liste des definitions.",
    response_model_by_alias=True,
)
async def composants_get(
    page_number: int = Header(0, description="Numero de la page voulue", ge=0),
    page_size: int = Header(
        25, description="Taille des pages, 0 pour page infini", ge=0),
) -> List[DeclarationComposant]:
    """Liste de tous les composants de la base de donnée"""
    async with pool.connection() as conn, conn.cursor(row_factory=class_row(DeclarationComposant)) as cur:
        await cur.execute('SELECT id, nom FROM definition_composant LIMIT %(limite)s OFFSET %(offset)s;', {'limite': page_size, 'offset': page_number*page_size})
        res = await cur.fetchall()
        return res
