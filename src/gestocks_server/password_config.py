import random
import string
from typing import Generator

scrypt_config = {
    "n": 16384,
    "r": 8,
    "p": 1
}


def salt_generator_base(num_bytes: int) -> Generator[bytes, None, None]:
    while True:
        random_bytes = bytes([random.randint(0, 255) for _ in range(num_bytes)])
        yield random_bytes


def tokken_generator_base(num_char: int) -> Generator[str, None, None]:
    caracteres = string.ascii_letters + string.digits
    while True:
        yield ''.join(random.choice(caracteres) for _ in range(num_char))
