from gestocks_server.database import config
from gestocks_server.models.extra_models import DERRole, sync_enum
from gestocks_server.password_config import scrypt_config, salt_generator_base
import psycopg as pg
from hashlib import scrypt

sync_enum()
with pg.connect(config) as conn, conn.cursor() as cur:

    sel = next(salt_generator_base(16))
    cur.execute("INSERT INTO users(nom, salt, psw_hash, ranks) VALUES(%s, %s, %s,%s);", [
                "Leopold", sel, scrypt("mot de passe".encode(), salt=sel, **scrypt_config), [_ for _ in DERRole]])
    sel = next(salt_generator_base(16))
    cur.execute("INSERT INTO users(nom, salt, psw_hash) VALUES(%s, %s, %s);", [
                "Bersani", sel, scrypt("mot de passe".encode(), salt=sel, **scrypt_config)])
