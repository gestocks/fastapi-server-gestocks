# coding: utf-8

from typing import List

from fastapi import Depends, Security  # noqa: F401
from fastapi.openapi.models import OAuthFlowImplicit, OAuthFlows  # noqa: F401
from fastapi.security import (  # noqa: F401
    HTTPAuthorizationCredentials,
    HTTPBasic,
    HTTPBasicCredentials,
    HTTPBearer,
    OAuth2,
    OAuth2AuthorizationCodeBearer,
    OAuth2PasswordBearer,
    SecurityScopes,
)
from fastapi.security.api_key import APIKeyCookie, APIKeyHeader, APIKeyQuery  # noqa: F401
from fastapi import HTTPException

from gestocks_server.models.extra_models import TokenModel, DERRole
from gestocks_server.database import pool

from psycopg.rows import dict_row

oauth2_password = OAuth2PasswordBearer(
    tokenUrl="login",
    scopes={
        'SITE_ADMIN': "Administrateur du site",
        'GESTION_MAGASIN': "Gestionaire du magasin",
        'GESTION_USERS': "Gestioanire des utilisateurs",
        'GESTION_STOCKS': "Utilisateur des stocks",
        'USER': "Utilisateur actif",
    }
)


async def get_token_oauthToken(
    security_scopes: SecurityScopes, token: str = Depends(oauth2_password)
) -> TokenModel | None:
    """
    Validate and decode token.

    :param: token Token provided by Authorization header
    :type token: str
    :return: Decoded token information or None if token is invalid
    :rtype: TokenModel | None
    """
    async with pool.connection() as conn, conn.cursor(row_factory=dict_row) as cur:
        await cur.execute("SELECT id, nom, ranks FROM session_tokken AS s JOIN users AS u ON s.users = u.id WHERE (s.tokken=%s) AND (NOW()<s.perish_date) AND NOT s.revoked ;", [token])
        ress = await cur.fetchall()
        if len(ress) == 1:
            res = ress[0]
            user_rank = DERRole.from_pgsql(res['ranks'])
            for scope_s in security_scopes.scopes:
                scope = DERRole.from_name(scope_s)
                if not scope & user_rank:
                    raise HTTPException(403, f"User not allowed, missing {scope.name}")
            return TokenModel(tokken=token, user_name=res['nom'], rank=user_rank, user_id=res['id'])
        raise HTTPException(403, "User not allowed")


def validate_scope_oauthToken(
    required_scopes: SecurityScopes, token_scopes: List[str]
) -> bool:
    """
    Validate required scopes are included in token scope

    :param required_scopes Required scope to access called API
    :type required_scopes: List[str]
    :param token_scopes Scope present in token
    :type token_scopes: List[str]
    :return: True if access to called API is allowed
    :rtype: bool
    """
    print(required_scopes)
    return False
