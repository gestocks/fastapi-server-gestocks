import psycopg_pool as pg_pool

config = "host=solaris dbname=dev_gestocks user=dev_gestocks password=12345"
sentry_report_url = "https://glet_bff37728e33022ea3e198515b2724bce@observe.gitlab.com:443/errortracking/api/v1/projects/39477966"
log_pool = pg_pool.AsyncConnectionPool(
    config, open=False, min_size=3, max_size=5, name='Logging pool')
pool = pg_pool.AsyncConnectionPool(
    config, open=False, min_size=3, max_size=5, name='Data pool')

pools = [log_pool, pool]
