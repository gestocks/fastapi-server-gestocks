# coding: utf-8

from __future__ import annotations
from datetime import date, datetime  # noqa: F401

import re  # noqa: F401
from typing import Any, Dict, List, Optional  # noqa: F401

from pydantic import AnyUrl, BaseModel, EmailStr, Field, validator  # noqa: F401
from gestocks_server.models.emplacement import Emplacement


class RepartitionInstanceContenuInner(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).

    Do not edit the class manually.

    RepartitionInstanceContenuInner - a model defined in OpenAPI

        nombre_disponible: The nombre_disponible of this RepartitionInstanceContenuInner.
        emplacement: The emplacement of this RepartitionInstanceContenuInner.
    """

    nombre_disponible: int = Field(alias="nombre_disponible")
    emplacement: Emplacement = Field(alias="emplacement")


RepartitionInstanceContenuInner.update_forward_refs()
