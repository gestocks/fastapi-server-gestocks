# coding: utf-8

from pydantic import BaseModel
from gestocks_server.database import config as db_config
import psycopg as pg
from enum import StrEnum, auto, Flag
from psycopg.types.enum import EnumInfo, register_enum
import re

pattern_role_pgsql_syntax_full = re.compile(r"{(([\w_]+),?)*}")
pattern_role = re.compile(r"[\w_]+")


class DERRole(Flag):
    SITE_ADMIN = auto()
    GESTION_MAGASIN = auto()
    GESTION_STOCKS = auto()
    GESTION_USERS = auto()
    USER = auto()

    @staticmethod
    def from_name(name: str) -> 'DERRole':
        try:
            tmp = getattr(DERRole, name)
            if isinstance(tmp, DERRole):
                return tmp
            raise ValueError
        except AttributeError:
            raise ValueError

    @staticmethod
    def from_pgsql(pg_string: str) -> 'DERRole':
        # Define the regular expression pattern to match the named group "role"

        # Use re.finditer to find all matches in the input string
        syntaxe_check = re.fullmatch(pattern_role_pgsql_syntax_full, pg_string)

        if syntaxe_check is None:
            raise ValueError

        matches = re.findall(pattern_role, pg_string)

        role = DERRole(0)
        for m in matches:
            role |= DERRole[m]
        return role


def sync_enum() -> None:
    with pg.connect(db_config) as conn:
        info = EnumInfo.fetch(conn, "der_rank")
        if info is None:
            raise TypeError
        else:
            register_enum(info, None, DERRole)


class TokenModel(BaseModel):
    """Defines a token model."""

    tokken: str
    user_name: str
    user_id: int
    rank: DERRole
